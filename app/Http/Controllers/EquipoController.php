<?php

namespace App\Http\Controllers;

use App\equipo;
use App\partido;
use App\gol;
use App\Http\Requests\equipo\StoreEquipoRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EquipoController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return response()->json(equipo::all(), 200);
//
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $equipos = equipo::all();
        if (count($equipos) < 32) {
            do {
                $grupo = rand(1, 8);
                $tmp = equipo::all()->where('fk_id_grupo', $grupo);
            } while (count($tmp) >= 4);
            $request->validate(
                    [
                'nombre' => 'required|string|max:50|unique:equipos',
                'escudo' => 'required|image'
                    ], [
                'nombre.required' => 'El campo nombre es requerido',
                'nombre.string' => 'El campo debe estar en un formato válido',
                'nombre.max' => 'El campo tiene demasiados carácteres',
                'nombre.unique' => 'El campo nombre ya esta registrado',
                'escudo.required' => 'El campo escudo es requerido',
                'escudo.image' => 'El campo debe tener un formato válido'
                    ]
            );
            if ($request->hasFile('escudo')) {
                $nameImagen = time() . $request->file('escudo')->getClientOriginalName();
                $request->file('escudo')->move('./../uploadImages', $nameImagen);
                $equipo = new equipo();
                $equipo->nombre = $request->nombre;
                $equipo->escudo = 'uploadImages/' . $nameImagen;
                $equipo->fk_id_grupo = $grupo;
                if ($equipo->save()) {
                    return response()->json(['message' => 'se agregó correctamente'], 200);
                } else {

                    return response()->json(['message' => 'ocurrió un error'], 500);
                }
            }
        } else {
            return response()->json(['message' => 'No se pueden agregar mas equipos'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function show(equipo $equipo) {
//
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function edit(equipo $equipo) {
//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, equipo $equipo) {
//
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\equipo  $equipo
     * @return \Illuminate\Http\Response
     */
    public function destroy(equipo $equipo) {
//
    }

    public function equipoGrupos(Request $request) {
        return response()->json(equipo::all()->where('fk_id_grupo', $request->id), 200);
    }

    public function tablaPosiciones() {
        $data = DB::table('equipos')
                        ->select('equipos.*', 'goles_equipos.goles_favor', 'goles_equipos.goles_contra')
                        ->join('goles_equipos', 'equipos.id', 'goles_equipos.fk_id_equipo'
                        )->get();
        $partidos = partido::all();
        $gol = gol::all();
        return response()->json(['equipos' => $data, 'partidos' => $partidos, 'goles' => $gol], 200);
    }

}
