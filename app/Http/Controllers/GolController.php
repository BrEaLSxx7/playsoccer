<?php

namespace App\Http\Controllers;

use App\gol;
use Illuminate\Http\Request;

class GolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return response()->json(gol::all(), 200);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $gol = new gol();
            $gol->fill($request->all());
            if ($gol->save()) {
                return response()->json(['message' => 'se agregó correctamente'], 200);
            } else {

                return response()->json(['message' => 'ocurrió un error'], 500);
            }
        } catch (Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 500);
        }
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\gol  $gol
     * @return \Illuminate\Http\Response
     */
    public function show(gol $gol)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\gol  $gol
     * @return \Illuminate\Http\Response
     */
    public function edit(gol $gol)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\gol  $gol
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, gol $gol)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\gol  $gol
     * @return \Illuminate\Http\Response
     */
    public function destroy(gol $gol)
    {
        //
    }
}
