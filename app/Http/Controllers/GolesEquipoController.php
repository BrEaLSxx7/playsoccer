<?php

namespace App\Http\Controllers;

use App\goles_equipo;
use Illuminate\Http\Request;

class GolesEquipoController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return response()->json(goles_equipo::all(), 200);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $request->validate([
            'id' => 'required|integer'
        ]);
        try {
            $goles_equipo = new goles_equipo();
//            $goles_equipo->fill($request->all());
            $goles_equipo->fk_id_equipo = $request->id;
            if ($goles_equipo->save()) {
                return response()->json(['message' => 'se agregó correctamente'], 200);
            } else {

                return response()->json(['message' => 'ocurrió un error'], 500);
            }
        } catch (Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 500);
        }
//
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\goles_equipo  $goles_equipo
     * @return \Illuminate\Http\Response
     */
    public function show(goles_equipo $goles_equipo) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\goles_equipo  $goles_equipo
     * @return \Illuminate\Http\Response
     */
    public function edit(goles_equipo $goles_equipo) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\goles_equipo  $goles_equipo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, goles_equipo $goles_equipo) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\goles_equipo  $goles_equipo
     * @return \Illuminate\Http\Response
     */
    public function destroy(goles_equipo $goles_equipo) {
        //
    }

}
