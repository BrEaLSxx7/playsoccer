<?php

namespace App\Http\Controllers;

use App\grupo;
use Illuminate\Http\Request;

class GrupoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(grupo::all(), 200);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $grupo = new grupo();
            $grupo->fill($request->all());
            if ($grupo->save()) {
                return response()->json(['message' => 'se agregó correctamente'], 200);
            } else {

                return response()->json(['message' => 'ocurrió un error'], 500);
            }
        } catch (Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 500);
        } //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\grupo  $grupo
     * @return \Illuminate\Http\Response
     */
    public function show(grupo $grupo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\grupo  $grupo
     * @return \Illuminate\Http\Response
     */
    public function edit(grupo $grupo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\grupo  $grupo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, grupo $grupo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\grupo  $grupo
     * @return \Illuminate\Http\Response
     */
    public function destroy(grupo $grupo)
    {
        //
    }
}
