<?php

namespace App\Http\Controllers;

use App\jugador;
use Illuminate\Http\Request;
use App\rol_posicion;
use Illuminate\Support\Facades\DB;

class JugadorController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return response()->json(jugador::all(), 200);
//
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $request->validate(
                [
            'nombre' => 'required|string|max:50',
            'primer_apellido' => 'nullable|string|max:50',
            'segundo_apellido' => 'nullable|string|max:50',
            'numero_camiseta' => 'required|integer',
            'numero_documento' => 'required|string',
            'fk_id_tpd' => 'nullable|integer',
            'fk_id_rol_posicion' => 'nullable|integer',
            'fk_id_equipo' => 'nullable|integer',
                ], [
            'nombre.required' => 'El campo nombre es requerido',
            'numero_documento.required' => 'El campo numero documento es requerido',
            'nombre.string' => 'El campo debe estar en un formato válido',
            'numero_documento.string' => 'El campo debe estar en un formato válido',
            'nombre.max' => 'El campo tiene demasiados carácteres',
            'primer_apellido.string' => 'El campo debe estar en un formato válido',
            'primer_apellido.max' => 'El campo tiene demasiados carácteres',
            'segundo_apellido.string' => 'El campo debe estar en un formato válido',
            'segundo_apellido.max' => 'El campo tiene demasiados carácteres',
            'numero_camiseta.integer' => 'El campo debe estar en un formato válido',
            'fk_id_tpd.integer' => 'El campo debe estar en un formato válido',
            'fk_id_rol_posicion.integer' => 'El campo debe estar en un formato válido',
            'fk_id_equipo.integer' => 'El campo debe estar en un formato válido',
                ]
        );
        try {
            $jugador = new jugador();
//            $jugador->fill($request->all());
            $jugador->nombre = $request->nombre;
            $jugador->primer_apellido = $request->primer_apellido;
            $jugador->segundo_apellido = $request->segundo_apellido;
            $jugador->fk_id_tpd = $request->fk_id_tpd;
            $jugador->fk_id_rol_posicion = $request->fk_id_rol_posicion;
            $jugador->numero_camiseta = $request->numero_camiseta;
            $jugador->numero_documento = $request->numero_documento;
            $jugador->fk_id_equipo = $request->fk_id_equipo;
            if ($jugador->save()) {
                return response()->json(['message' => 'se agregó correctamente'], 200);
            } else {

                return response()->json(['message' => 'ocurrió un error'], 500);
            }
        } catch (Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 500);
        }// //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\jugador  $jugador
     * @return \Illuminate\Http\Response
     */
    public function show(jugador $jugador) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\jugador  $jugador
     * @return \Illuminate\Http\Response
     */
    public function edit(jugador $jugador) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\jugador  $jugador
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, jugador $jugador) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\jugador  $jugador
     * @return \Illuminate\Http\Response
     */
    public function destroy(jugador $jugador) {
        //
    }

    public function jugadorEquipos(Request $request) {
        $data = DB::table('jugadors')
                ->select('jugadors.*', 'rol_posicions.nombre as posiciones')
                ->join('rol_posicions', 'jugadors.fk_id_rol_posicion', '=', 'rol_posicions.id')
                ->where('jugadors.fk_id_equipo', $request->id)
                ->get();
        return response()->json($data, 200);
    }

}
