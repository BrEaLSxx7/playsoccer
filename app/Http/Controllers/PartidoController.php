<?php

namespace App\Http\Controllers;

use App\partido;
use App\equipo;
use App\gol;
use App\tarjeta;
use App\jugador;
use App\goles_equipo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PartidoController extends Controller {

    private $goles_local = 0;
    private $goles_visitante = 0;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
//        $dataLocal = DB::table('partidos')
//                ->join('equipos', 'partidos.fk_id_equipo_local', '=', 'equipos.id')
//                ->get();
//        $dataVisitante = DB::table('partidos')
//                ->join('equipos', 'partidos.fk_id_equipo_visitante', '=', 'equipos.id')
//                ->get();
//        $data = array_merge($dataLocal->toArray(), $dataVisitante->toArray());
        return response()->json(['equipos' => equipo::all(), 'partidos' => partido::all()], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
//        var_dump($request->all());
        $partido = new partido();
        $partido->fk_id_equipo_local = $request->equipo1;
        $partido->fk_id_equipo_visitante = $request->equipo2;
        $partido->save();

        for ($i = 0; $i < count($request->objetos); $i ++) {
            if ($request->objetos[$i]['gol'] === 'true') {
                $this->addGol($request->objetos[$i]['jugador'], $partido->id);
            } else {
                $this->addtarget($request->objetos[$i]['jugador'], $partido->id, $request->objetos[$i]['tarjeta_amarilla'], $request->objetos[$i]['tarjeta_roja']);
            }
        }
        if ($request->goles_local > $request->goles_visitante) {
            $equipo = equipo::all()->firstWhere('id', $request->equipo1);
            $equipo->puntos += 3;
            $equipo->save();
        } else if ($request->goles_local < $request->goles_visitante) {
            $equipo = equipo::all()->firstWhere('id', $request->equipo2);
            $equipo->puntos += 3;
            $equipo->save();
        } else if ($request->goles_local === $request->goles_visitante) {
            $equipo = equipo::all()->firstWhere('id', $request->equipo1);
            $equipo->puntos += 3;
            $equipo->save();


            $equipo2 = equipo::all()->firstWhere('id', $request->equipo2);
            $equipo2->puntos += 3;
            $equipo2->save();
        }

        $gequipo = goles_equipo::all()->firstWhere('fk_id_equipo', $request->equipo1);
        $gequipo->goles_favor += $request->goles_local;
        $gequipo->goles_contra += $request->goles_visitante;
        $gequipo->save();
        $gequipo2 = goles_equipo::all()->firstWhere('fk_id_equipo', $request->equipo2);
        $gequipo2->goles_favor += $request->goles_visitante;
        $gequipo2->goles_contra += $request->goles_local;
        $gequipo2->save();
        return response()->json(['message' => 'se agregó correctamente'], 200);
    }

    private function addGol(int $jugador, int $partido) {
        $gol = new gol();
        $gol->fk_id_jugador = $jugador;
        $gol->fk_id_partido = $partido;
        $gol->save();
    }

    private function addtarget($jugador, $partido, $amarilla, $roja) {
        $tarjeta = new tarjeta();
        $tarjeta->fk_id_jugador = $jugador;
        $tarjeta->fk_id_partido = $partido;

        if ($amarilla === 'true') {
            $tarjeta->tarjeta_amarilla = true;
            $tarjeta->tarjeta_roja = false;
        } else {
            $tarjeta->tarjeta_amarilla = false;
            $tarjeta->tarjeta_roja = true;
        }
        $tarjeta->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\partido  $partido
     * @return \Illuminate\Http\Response
     */
    public function show(partido $partido) {
//
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\partido  $partido
     * @return \Illuminate\Http\Response
     */
    public function edit(partido $partido) {
//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\partido  $partido
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, partido $partido) {
//
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\partido  $partido
     * @return \Illuminate\Http\Response
     */
    public function destroy(partido $partido) {
//
    }

    public function verify() {
        $equipos = equipo::all();
        $bool = false;
        $data = [
        ];
        if (count($equipos) >= 32) {
            foreach ($equipos as $value) {
                $jugadores = jugador::all()->
                        where('fk_id_equipo', $value['id']);
                $bool = ( count($jugadores) >= 11) ? true : false;
                if (!$bool) {
                    $faltan = 11 -
                            count($jugadores);
                    $data = $value;
                    break;
                    return

                                    response()->
                                    json(['message' => "En el equipo " . $data['nombre'] . " le hacen falta " . $faltan . " jugadores", 'show' => $bool], 200);
                } else {
                    return

                            response()->json(['message' => "ok", 'show' => $bool], 200);
                }
            }
        }
    }

}
