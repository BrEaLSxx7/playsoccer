<?php

namespace App\Http\Controllers;

use App\rol_posicion;
use Illuminate\Http\Request;

class RolPosicionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(rol_posicion::all(), 200);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       try {
            $rol_posicion = new rol_posicion();
            $rol_posicion->fill($request->all());
            if ($rol_posicion->save()) {
                return response()->json(['message' => 'se agregó correctamente'], 200);
            } else {

                return response()->json(['message' => 'ocurrió un error'], 500);
            }
        } catch (Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 500);
        }
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\rol_posicion  $rol_posicion
     * @return \Illuminate\Http\Response
     */
    public function show(rol_posicion $rol_posicion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\rol_posicion  $rol_posicion
     * @return \Illuminate\Http\Response
     */
    public function edit(rol_posicion $rol_posicion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\rol_posicion  $rol_posicion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, rol_posicion $rol_posicion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\rol_posicion  $rol_posicion
     * @return \Illuminate\Http\Response
     */
    public function destroy(rol_posicion $rol_posicion)
    {
        //
    }
}
