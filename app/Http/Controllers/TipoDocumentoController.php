<?php

namespace App\Http\Controllers;

use App\tipo_documento;
use Illuminate\Http\Request;

class TipoDocumentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(tipo_documento::all(), 200);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       try {
            $tipo_documento = new tipo_documento();
            $tipo_documento->fill($request->all());
            if ($tipo_documento->save()) {
                return response()->json(['message' => 'se agregó correctamente'], 200);
            } else {

                return response()->json(['message' => 'ocurrió un error'], 500);
            }
        } catch (Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 500);
        }
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tipo_documento  $tipo_documento
     * @return \Illuminate\Http\Response
     */
    public function show(tipo_documento $tipo_documento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tipo_documento  $tipo_documento
     * @return \Illuminate\Http\Response
     */
    public function edit(tipo_documento $tipo_documento)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tipo_documento  $tipo_documento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tipo_documento $tipo_documento)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tipo_documento  $tipo_documento
     * @return \Illuminate\Http\Response
     */
    public function destroy(tipo_documento $tipo_documento)
    {
        //
    }
}
