<?php

namespace App\Http\Controllers;

use App\usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsuarioController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return response()->json(usuario::all(), 200);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $request->validate(
                [
            'numero_documento' => 'required|string|max:50|unique:usuarios',
            'password' => 'required|string|max:60',
            'fk_id_tpd' => 'required|integer',
            'fk_id_rol' => 'required|integer',
                ], [
            'numero_documento.required' => 'El campo es requerido',
            'numero_documento.string' => 'El campo debe estar en un formato válido',
            'numero_documento.max' => 'El campo tiene demasiados carácteres',
            'numero_documento.unique' => 'El campo nombre ya esta registrado',
            'password.required' => 'El campo es requerido',
            'fk_id_tpd.required' => 'El campo es requerido',
            'fk_id_rol.required' => 'El campo es requerido',
            'password.string' => 'El campo debe estar en un formato válido',
            'password.max' => 'El campo tiene demasiados carácteres',
            'fk_id_tpd.integer' => 'El campo debe estar en un formato válido',
            'fk_id_rol.integer' => 'El campo debe estar en un formato válido',
        ]);
        try {
            $usuario = new usuario();
//            $usuario->fill($request->all());
            $usuario->numero_documento = $request->numero_documento;
            $usuario->password = Hash::make($request->password);
            $usuario->fk_id_tpd = $request->fk_id_tpd;
            $usuario->fk_id_rol = $request->fk_id_rol;
            $usuario->usu_token = str_random(60);
            if ($usuario->save()) {
                return response()->json(['message' => 'se agregó correctamente'], 200);
            } else {

                return response()->json(['message' => 'ocurrió un error'], 500);
            }
        } catch (Exception $ex) {
            return response()->json(['error' => $ex->getMessage()], 500);
        }
//
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function show(usuario $usuario) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function edit(usuario $usuario) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, usuario $usuario) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(usuario $usuario) {
        //
    }

    public function auth(Request $request) {

        $request->validate([
            'numero_documento' => 'required|string|max:50|exists:usuarios',
            'password' => 'required|max:100',
            'fk_id_tpd' => 'required|integer'
                ], [
            'numero_documento.required' => 'Todos los campos son requeridos',
            'numero_documento.string' => 'El campo debe tener un formato valido',
            'numero_documento.max' => 'El campo numero documento tiene demasiados caracteres',
            'numero_documento.exists' => 'Datos incorrectos',
            'password.required' => 'Todos los campos son requeridos',
            'password.max' => 'El campo contraseña tiene demasiados caracteres',
            'fk_id_tpd.required' => 'Todos los campos son requeridos',
            'fk_id_tpd.integer' => 'El campo debe tener un formato valido'
        ]);
        $user = usuario::where('numero_documento', $request->numero_documento)->firstOrFail();
        if (Hash::check($request->password, $user->password) && $user->fk_id_tpd === $request->fk_id_tpd) {
            return response()->json(['data' => $user], 200);
        } else {
            return response()->json(['message' => 'Datos incorrectos'], 401);
        }
    }

}
