<?php

namespace App\Http\Requests\equipo;

use Illuminate\Foundation\Http\FormRequest;

class StoreEquipoRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'nombre' => 'required|string|max:50|unique:equipos',
            'escudo' => 'required|image'
        ];
    }

    public function messages(): array {
        return [
            'nombre.required' => 'El campo nombre es requerido',
            'nombre.string' => 'El campo debe estar en un formato válido',
            'nombre.max' => 'El campo tiene demasiados carácteres',
            'nombre.unique' => 'El campo nombre ya esta registrado',
            'escudo.required' => 'El campo escudo es requerido',
            'escudo.image' => 'El campo debe tener un formato válido'
        ];
    }

}
