<?php

namespace App\Http\Requests\jugador;

use Illuminate\Foundation\Http\FormRequest;

class StoreJugadorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|string|max:50',
            'primer_apellido' => 'nullable|string|max:50',
            'segundo_apellido' => 'nullable|string|max:50',
            'numero_camiseta' => 'nullable|integer', 
            'fk_id_tpd'=> 'nullable|integer',
            'fk_id_rol_posicion' => 'nullable|integer',
            'fk_id_equipo' => 'nullable|integer',    
        ];
    }
    
    public function messages(): array {
        return [
            'nombre.required' => 'El campo nombre es requerido',
            'nombre.string' => 'El campo debe estar en un formato válido',
            'nombre.max'=> 'El campo tiene demasiados carácteres',
            'primer_apellido.string' => 'El campo debe estar en un formato válido',
            'primer_apellido.max' => 'El campo tiene demasiados carácteres',
            'segundo_apellido.string' => 'El campo debe estar en un formato válido',
            'segundo_apellido.max' => 'El campo tiene demasiados carácteres',
            'numero_camiseta.integer'=> 'El campo debe estar en un formato válido',
            'fk_id_tpd.integer'=> 'El campo debe estar en un formato válido',
            'fk_id_rol_posicion.integer'=> 'El campo debe estar en un formato válido',
            'fk_id_equipo.integer'=> 'El campo debe estar en un formato válido',
        ];
    }
}
