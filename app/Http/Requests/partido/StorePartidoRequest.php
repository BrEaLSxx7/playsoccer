<?php

namespace App\Http\Requests\partido;

use Illuminate\Foundation\Http\FormRequest;

class StorePartidoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           
            'fk_id_equipo_local' => 'nullable|integer',
            'fk_id_equipo_visitante' => 'nullable|integer',
            
        ];
    }
    public function messages(): array {
        return [
            'fk_id_equipo_local.integer' => 'El campo debe estar en un formato válido',
            'fk_id_equipo_visitante.integer' => 'El campo debe estar en un formato válido',
           
        ];
    }
}
