<?php

namespace App\Http\Requests\usuario;

use Illuminate\Foundation\Http\FormRequest;

class StoreUsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'numero_documento' => 'required|string|max:50|unique:usuarios',
            'password' => 'required|string|max:60',
            'fk_id_tpd'=> 'nullable|integer',
            'fk_id_rol' => 'nullable|integer',
        ];
    }
    
    public function messages(): array {
        return [
            'numero_documento.required' => 'El campo nombre es requerido',
            'numero_documento.string' => 'El campo debe estar en un formato válido',
            'numero_documento.max' => 'El campo tiene demasiados carácteres',
            'numero_documento.unique' => 'El campo nombre ya esta registrado',
            'password.required' => 'El campo nombre es requerido',
            'password.string' => 'El campo debe estar en un formato válido',
            'password.max'=> 'El campo tiene demasiados carácteres',
            'fk_id_tpd.integer'=> 'El campo debe estar en un formato válido',
            'fk_id_rol.integer'=> 'El campo debe estar en un formato válido',
        ];
    }
}
