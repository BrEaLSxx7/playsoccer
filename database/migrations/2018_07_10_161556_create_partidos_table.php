<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartidosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('partidos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_id_equipo_local')->unsigned();
            $table->integer('fk_id_equipo_visitante')->unsigned();
            $table->timestamps();

            $table->foreign('fk_id_equipo_local')->references('id')->on('equipos')->onDelete('no action')->onUpdate('no action');
            $table->foreign('fk_id_equipo_visitante')->references('id')->on('equipos')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('partidos');
    }

}
