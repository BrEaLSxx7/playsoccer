<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJugadorsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('jugadors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 50);
            $table->string('primer_apellido', 50)->nullable();
            $table->string('segundo_apellido', 50)->nullable();
            $table->integer('numero_camiseta')->unsigned();
            $table->string('numero_documento', 25)->unique();
            $table->integer('fk_id_tpd')->unsigned();
            $table->integer('fk_id_rol_posicion')->unsigned();
            $table->integer('fk_id_equipo')->unsigned();
            $table->timestamps();


            $table->foreign('fk_id_tpd')->references('id')->on('tipo_documentos')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('fk_id_rol_posicion')->references('id')->on('rol_posicions')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('fk_id_equipo')->references('id')->on('equipos')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('jugadors');
    }

}
