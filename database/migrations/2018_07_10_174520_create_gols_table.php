<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGolsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('gols', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_id_jugador')->unsigned();
            $table->integer('fk_id_partido')->unsigned();
            $table->timestamps();

            $table->foreign('fk_id_jugador')->references('id')->on('jugadors')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('fk_id_partido')->references('id')->on('partidos')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('gols');
    }

}
