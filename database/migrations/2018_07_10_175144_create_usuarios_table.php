<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_documento', 50)->unique();
            $table->string('password', 60);
            $table->string('usu_token', 60)->unique();
            $table->integer('fk_id_tpd')->unsigned();
            $table->integer('fk_id_rol')->unsigned();
            $table->timestamps();

            $table->foreign('fk_id_rol')->references('id')->on('rols')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('fk_id_tpd')->references('id')->on('tipo_documentos')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('usuarios');
    }

}
