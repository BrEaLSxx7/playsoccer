<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGolesEquiposTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('goles_equipos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('goles_favor')->unsigned()->default(0);
            $table->integer('goles_contra')->unsigned()->default(0);
            $table->integer('fk_id_equipo')->unsigned();
            $table->timestamps();


            $table->foreign('fk_id_equipo')->references('id')->on('equipos')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('goles_equipos');
    }

}
