<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTarjetasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tarjetas', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('tarjeta_amarilla')->nullable();
            $table->boolean('tarjeta_roja')->nullable();
            $table->unsignedInteger('fk_id_jugador');
            $table->integer('fk_id_partido')->unsigned();
            $table->timestamps();

            $table->foreign('fk_id_jugador')->references('id')->on('jugadors')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('fk_id_partido')->references('id')->on('partidos')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('tarjetas');
    }

}
