<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        $this->call(GruposSeeder::class);
        $this->call(RolPosicionsSeeder::class);
        $this->call(RolsSeeder::class);
        $this->call(TipoDocumentoSeeder::class);
        $this->call(UsuariosSeeder::class);
    }

}
