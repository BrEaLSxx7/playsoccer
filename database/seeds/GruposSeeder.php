<?php

use Illuminate\Database\Seeder;

class GruposSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('grupos')->insert([
            'nombre' => 'A'
        ]);
        DB::table('grupos')->insert([
            'nombre' => 'B'
        ]);
        DB::table('grupos')->insert([
            'nombre' => 'C'
        ]);
        DB::table('grupos')->insert([
            'nombre' => 'D'
        ]);
        DB::table('grupos')->insert([
            'nombre' => 'E'
        ]);
        DB::table('grupos')->insert([
            'nombre' => 'F'
        ]);
        DB::table('grupos')->insert([
            'nombre' => 'G'
        ]);
        DB::table('grupos')->insert([
            'nombre' => 'H'
        ]);
    }

}
