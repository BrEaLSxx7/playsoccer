<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolPosicionsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        DB::table('rol_posicions')->insert([
            'nombre' => 'PORTERO'
        ]);
        DB::table('rol_posicions')->insert([
            'nombre' => 'DEFENSA'
        ]);
        DB::table('rol_posicions')->insert([
            'nombre' => 'MEDIOCAMPO'
        ]);
        DB::table('rol_posicions')->insert([
            'nombre' => 'ATACANTE'
        ]);
    }

}
