<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        DB::table('rols')->insert([
            'nombre' => 'Administrador'
        ]);
        DB::table('rols')->insert([
            'nombre' => 'Delegado'
        ]);
    }

}
