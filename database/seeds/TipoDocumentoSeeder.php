<?php

use Illuminate\Database\Seeder;

class TipoDocumentoSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('tipo_documentos')->insert([
            'nombre' => 'Cédula'
        ]);
        DB::table('tipo_documentos')->insert([
            'nombre' => 'Tarjeta Identidad'
        ]);
        DB::table('tipo_documentos')->insert([
            'nombre' => 'Cédula Extranjeria'
        ]);
        DB::table('tipo_documentos')->insert([
            'nombre' => 'Pasaporte'
        ]);
    }

}
