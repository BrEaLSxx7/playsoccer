<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::apiResource('rols', 'RolController');
Route::apiResource('tpd', 'TipoDocumentoController');
Route::apiResource('equipo', 'EquipoController');
Route::apiResource('gol', 'GolController');
Route::apiResource('golequipo', 'GolesEquipoController');
Route::apiResource('grupo', 'GrupoController');
Route::apiResource('jugador', 'JugadorController');
Route::apiResource('partido', 'PartidoController');
Route::apiResource('rolposicion', 'RolPosicionController');
Route::apiResource('usuario', 'UsuarioController');

Route::get('partidos/verify', 'PartidoController@verify');
Route::get('jugadores/equipo', 'JugadorController@jugadorEquipos');
Route::get('equipos/grupo', 'EquipoController@equipoGrupos');
Route::get('tablas/posiciones', 'EquipoController@tablaPosiciones');
Route::post('usuarios/auth', 'UsuarioController@auth')->name('usuarios.auth');
